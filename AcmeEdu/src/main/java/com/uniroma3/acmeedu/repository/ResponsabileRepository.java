package com.uniroma3.acmeedu.repository;

import org.springframework.data.repository.CrudRepository;

import com.uniroma3.acmeedu.model.Responsabile;

public interface ResponsabileRepository extends CrudRepository<Responsabile, Long>{

}
