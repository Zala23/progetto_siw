package com.uniroma3.acmeedu.repository;

import org.springframework.data.repository.CrudRepository;

import com.uniroma3.acmeedu.model.Attivita;

public interface AttivitaRepository extends CrudRepository<Attivita, Long> {

	Attivita findByNome(String nome);
	
	Attivita findByCodice(String codice);

}
