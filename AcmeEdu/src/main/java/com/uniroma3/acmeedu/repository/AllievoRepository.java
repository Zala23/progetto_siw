package com.uniroma3.acmeedu.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.uniroma3.acmeedu.model.Allievo;

public interface AllievoRepository extends CrudRepository<Allievo, Long> {

	List<Allievo> findByNomeAndCognome(String nome, String cognome);

	Allievo findByTelefono(String telefono);	
}
