package com.uniroma3.acmeedu.repository;

import org.springframework.data.repository.CrudRepository;

import com.uniroma3.acmeedu.model.Centro;

public interface CentroRepository extends CrudRepository<Centro, Long> {
	
}
