package com.uniroma3.acmeedu.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uniroma3.acmeedu.model.Responsabile;
import com.uniroma3.acmeedu.repository.ResponsabileRepository;

@Transactional
@Service
public class ResponsabileService {
	
	@Autowired
	private ResponsabileRepository responsabileRepository;
	
	public Responsabile save(Responsabile responsabile) {
		return responsabileRepository.save(responsabile);
	}
	
	public void delete(Responsabile responsabile) {
		responsabileRepository.delete(responsabile);
	}
	
	public Responsabile findById(Long id) {
		return responsabileRepository.findById(id).get();
	}
	
	public List<Responsabile> findAll() {
		return (List<Responsabile>) responsabileRepository.findAll();
	}
}
