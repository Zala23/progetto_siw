package com.uniroma3.acmeedu.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uniroma3.acmeedu.model.Attivita;
import com.uniroma3.acmeedu.repository.AttivitaRepository;


@Transactional
@Service 
public class AttivitaService {
	
	@Autowired
	private AttivitaRepository attivitaRepository;
	
	public Attivita save(Attivita attivita) {
		return this.attivitaRepository.save(attivita);
	}
	
	public Attivita findById(Long id) {
		Optional<Attivita> customer = this.attivitaRepository.findById(id);
		if (customer.isPresent()) 
			return customer.get();
		else
			return null;
	}
	
	public List<Attivita> findAll() {
		return (List<Attivita>) this.attivitaRepository.findAll();
	}
	
	public void delete(Attivita attivita) {
		this.attivitaRepository.delete(attivita);
	}

	public Attivita findByNome(String attivita) {
		return this.attivitaRepository.findByNome(attivita);
	}

	public boolean alreadyExists(Attivita attivita) {
		return (this.attivitaRepository.findByCodice(attivita.getCodice()) == null);
	}

	public Attivita findByCodice(String codice) {
		return this.attivitaRepository.findByCodice(codice);
	}	
}
