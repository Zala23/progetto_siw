package com.uniroma3.acmeedu.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uniroma3.acmeedu.model.Allievo;
import com.uniroma3.acmeedu.model.Attivita;
import com.uniroma3.acmeedu.repository.AllievoRepository;

@Transactional
@Service
public class AllievoService {
	
	@Autowired
	private AllievoRepository allievoRepository;
	
	public void add(Allievo allievo) {
		allievoRepository.save(allievo);
	}
	
	public Allievo findById(Long id) {
		return allievoRepository.findById(id).get();
	}
	
	public void delete(Allievo allievo) {
		allievoRepository.delete(allievo);
	}
	
	public List<Allievo> findAll() {
		return (List<Allievo>) allievoRepository.findAll();
	}
	
	public boolean alreadyExist(Allievo allievo) {
		List<Allievo> customers = this.allievoRepository.findByNomeAndCognome(allievo.getNome(), allievo.getCognome());
		return (customers.size() > 0);
	}
	
	public void cancella(Allievo allievo) {
		allievoRepository.delete(allievo);
	}

	public Allievo findByTelefono(String telefono) {
		return allievoRepository.findByTelefono(telefono);
	}

	public void associa(Allievo allievo, Attivita attivita) {
		allievo.getAttivita().add(attivita);
		attivita.getAllievi().add(allievo);
	}
}
