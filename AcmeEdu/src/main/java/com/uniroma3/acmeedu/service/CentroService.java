package com.uniroma3.acmeedu.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uniroma3.acmeedu.model.Centro;
import com.uniroma3.acmeedu.repository.CentroRepository;

@Transactional
@Service
public class CentroService {

	@Autowired
	private CentroRepository centroRepository;
	
	public Centro save(Centro centro) {
		return centroRepository.save(centro);
	}
	
	public void delete(Centro centro) {
		centroRepository.delete(centro);
	}
	
	public Centro findById(Long id) {
		return centroRepository.findById(id).get();
	}
	
	public List<Centro> findAll() {
		return (List<Centro>) centroRepository.findAll();
	}
}
