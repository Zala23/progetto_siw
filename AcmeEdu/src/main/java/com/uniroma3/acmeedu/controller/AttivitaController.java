package com.uniroma3.acmeedu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.uniroma3.acmeedu.controller.validator.AttivitaValidator;
import com.uniroma3.acmeedu.model.Attivita;
import com.uniroma3.acmeedu.service.AttivitaService;

@Controller
public class AttivitaController {
	@Autowired
	private AttivitaService attivitaService;

	@Autowired
	private AttivitaValidator validator;

	@RequestMapping("/aggiungiAttivita")
	public String aggiungiAttività(Model model) {
		model.addAttribute("attivita", new Attivita());

		return "attivitaForm";
	}

	@RequestMapping(value = "/attivita", method = RequestMethod.POST)
	public String nuovaAttivita(@ModelAttribute("attivita") Attivita attivita, Model model,
			BindingResult bindingResult) {
		this.validator.validate(attivita, bindingResult);

		if (!this.attivitaService.alreadyExists(attivita)) {
			model.addAttribute("esiste", "Attività esistente");
			return "attivitaForm";
		} else {
			if (!bindingResult.hasErrors()) {
				this.attivitaService.save(attivita);
				model.addAttribute("attivita", this.attivitaService.findAll());
				return "attivita";
			}
		}

		return "attivitaForm";
	}

	@RequestMapping(value = "/attivita/{id}", method = RequestMethod.GET) 
	public String getAttivita(@PathVariable("id") Long id, Model model) {
		model.addAttribute("attivita", this.attivitaService.findById(id));
		
		return "mostraAttivita";
	}
	
	@RequestMapping("/listaAttivita")
	public String getListaAttivita(Model model) {
		model.addAttribute("attivita", this.attivitaService.findAll());
		
		return "attivita";
	}
}
