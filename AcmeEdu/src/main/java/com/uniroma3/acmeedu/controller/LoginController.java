package com.uniroma3.acmeedu.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;

@Controller
public class LoginController {
	private Map<String, String> credenziali = new HashMap<>();

	public boolean controllaCredenziali(String user, String pass) {
		return (credenziali.containsKey(user) && credenziali.get(user) == pass);
	}
}
