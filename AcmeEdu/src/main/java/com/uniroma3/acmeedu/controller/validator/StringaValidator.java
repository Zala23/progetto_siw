package com.uniroma3.acmeedu.controller.validator;

import org.springframework.stereotype.Component;

@Component
public class StringaValidator {
	public boolean soloNumeri(String stringa) {
		for(int i = 0; i < (stringa.length() - 1); i++) 
			if((stringa.charAt(i) >= 'a' && stringa.charAt(i) <= 'z') || (stringa.charAt(i) >= 'A' && stringa.charAt(i) <= 'Z'))
				return false;
		
		return true;
	}
}
