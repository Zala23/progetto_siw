package com.uniroma3.acmeedu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.uniroma3.acmeedu.service.AllievoService;
import com.uniroma3.acmeedu.service.AttivitaService;

@Controller
public class CentroController {
	@Autowired
	private AllievoService allievoService;
	
	@Autowired
	private AttivitaService attivitaService;
	
	@RequestMapping("/associazioneAllievoAttività")
	public String getAssociazioneForm() {
		return "associazioneAllievoAttivitaForm";
	}
	
	@RequestMapping(value = "/associaAttivita", method = RequestMethod.POST)
	public String nuovaAssociazione(@RequestParam("telefono") String telefono,
			@RequestParam("attivita") String attivita, Model model) {

		if (telefono == "" && attivita == "") {
			model.addAttribute("obbligatorioT", "Telefono è un campo obbligatorio");
			model.addAttribute("obbligatorioA", "Nome Attività è un campo obbligatorio");

			return "associazioneAllievoAttivitaForm";
		} else if (this.allievoService.findByTelefono(telefono).getAttivita()
				.contains(this.attivitaService.findByNome(attivita))) {
			model.addAttribute("esiste", "L'allievo è stato già assocciato a quest'attività");

			return "associazioneAllievoAttivitaForm";

		} else {
			if ((telefono != null && attivita != null)) {
				this.allievoService.associa(this.allievoService.findByTelefono(telefono),
						this.attivitaService.findByNome(attivita));
				model.addAttribute("allievi", this.attivitaService.findByNome(attivita).getAllievi());

				return "listaAllieviAttivita";
			}

			return "associazioneAllievoAttivitaForm";
		}
	}
}
