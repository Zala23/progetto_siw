package com.uniroma3.acmeedu.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.uniroma3.acmeedu.controller.validator.AllievoValidator;
import com.uniroma3.acmeedu.controller.validator.StringaValidator;
import com.uniroma3.acmeedu.model.Allievo;
import com.uniroma3.acmeedu.service.AllievoService;

@Controller
public class AllievoController {
	@Autowired
	private AllievoService allievoService;

	@Autowired
	private AllievoValidator validator;

	@Autowired
	private StringaValidator svalidator;

	@RequestMapping("/aggiungiAllievo")
	public String aggiungiAllievo(Model model) {
		model.addAttribute("allievo", new Allievo());

		return "allievoForm";
	}

	@RequestMapping("/rimuoviAllievo")
	public String paginaCancellaAllievo() {
		return "rimozioneForm";
	}

	@RequestMapping("/cancellaAllievo")
	public String cancellaAllievo(@RequestParam("telefono") String telefono, Model model) {
		if (!svalidator.soloNumeri(telefono)) {
			model.addAttribute("nonNumero", "Il campo telefono può contenere solo numeri");
			return "rimozioneForm";
		} else {
			if (telefono == "") {
				model.addAttribute("obbligatorio", "Telefono è un campo obbligatorio");
				return "rimozioneForm";
			} else {
				if (this.allievoService.findByTelefono(telefono) == null) {
					model.addAttribute("nonEsiste", "L'allievo non è presente nel sistema");
					return "rimozioneForm";
				} else {
					this.allievoService.cancella(this.allievoService.findByTelefono(telefono));
					model.addAttribute("allievi", this.allievoService.findAll());
					return "listaAllievi";
				}
			}
		}
	}

	@RequestMapping("/allievi")
	public String allievi(Model model) {
		model.addAttribute("allievi", this.allievoService.findAll());
		return "listaAllievi";
	}

	@RequestMapping(value = "/allievo/{telefono}", method = RequestMethod.GET)
	public String getAllievo(@PathVariable("telefono") String telefono, Model model) {
		model.addAttribute("allievo", this.allievoService.findByTelefono(telefono));
		return "mostraAllievo";
	}

	@RequestMapping(value = "/allievo", method = RequestMethod.POST)
	public String nuovoAllievo(@Valid @ModelAttribute("allievo") Allievo allievo, Model model,
			BindingResult bindingResult) {
		this.validator.validate(allievo, bindingResult);

		if (this.allievoService.findByTelefono(allievo.getTelefono()) != null) {
			model.addAttribute("esiste", "Allievo già presente");
			return "allievoForm";
		} else {
			if (!bindingResult.hasErrors()) {
				this.allievoService.add(allievo);
				model.addAttribute("allievi", this.allievoService.findAll());
				return "listaAllievi";
			}
		}

		return "allievoForm";
	}

	

	
}
