package com.uniroma3.acmeedu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class IndexController {
//	@Autowired
//	private LoginController loginController;
	
	@RequestMapping(value = "/mainPage", method = RequestMethod.POST)
	public String getMainPage(@RequestParam("user") String user, @RequestParam("password") String password) {
		//if(loginController.controllaCredenziali(user, password))
			return "mainPage";
		//else
		//	return "loginPage";
	}
	
	@RequestMapping(value = "/mainPage", method = RequestMethod.GET)
	public String ariGetMainPage() {
		return "mainPage";
	}
}
