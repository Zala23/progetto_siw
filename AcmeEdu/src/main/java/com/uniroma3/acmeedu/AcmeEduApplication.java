package com.uniroma3.acmeedu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableJpaRepositories("it.acmeedu.repository")
//@EntityScan("it.acmeedu.model")
//@ComponentScan("it.uniroma3.controller.validator")
public class AcmeEduApplication {
	public static void main (String[] args) {
		SpringApplication.run(AcmeEduApplication.class, args);
	}	
}
