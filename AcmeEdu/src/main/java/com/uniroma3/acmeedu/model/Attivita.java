package com.uniroma3.acmeedu.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Attivita {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column
	private String codice;
	@Column(nullable = false)
	private String nome;
	@Column(nullable = false)
	private String data;
	@Column(nullable = false)
	private String ora;
	@ManyToMany
	private List<Allievo> allievi = new ArrayList<>();

	// public Attivita(String nome) {
	// this.nome = nome;
	// }

	public Attivita() {
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String identificativo) {
		this.codice = identificativo;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	public List<Allievo> getAllievi() {
		return this.allievi;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getOra() {
		return ora;
	}

	public void setOra(String ora) {
		this.ora = ora;
	}
}
