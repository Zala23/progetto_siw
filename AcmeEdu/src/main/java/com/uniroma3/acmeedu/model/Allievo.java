package com.uniroma3.acmeedu.model;

import java.util.ArrayList;
// import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Allievo {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(nullable = false)
	private String nome;
	@Column(nullable = false)
	private String cognome;
	@Column(nullable = false)
	private String email;
	@Column(nullable = false)
	private String telefono;
	@Column(nullable = false)
	private String dataNascita;
	@Column(nullable = false)
	private String luogoNascita;
	@ManyToMany(mappedBy = "allievi", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	private List<Attivita> attivita = new ArrayList<>();
	@ManyToMany(mappedBy = "allieviAffiliati", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	private List<Centro> centriAffiliato = new ArrayList<>(); 

	public Allievo() {}
	
	public Allievo(String nome, String cognome, String email, String telefono, String data_di_nascita, String luogo_di_nascita) {
		this.nome = nome;
		this.cognome = cognome;
		this.email = email;
		this.telefono = telefono;
		this.dataNascita = data_di_nascita;
		this.luogoNascita = luogo_di_nascita;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(String dataNascita) {
		this.dataNascita = dataNascita;
	}

	public String getLuogoNascita() {
		return luogoNascita;
	}

	public void setLuogoNascita(String luogoNascita) {
		this.luogoNascita = luogoNascita;
	}

	public Long getId() {
		return id;
	}
	
	public List<Attivita> getAttivita() {
		return attivita;
	}

	public void setAttivita(List<Attivita> attivita) {
		this.attivita = attivita;
	}
}
