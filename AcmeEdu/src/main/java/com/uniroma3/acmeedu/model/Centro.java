package com.uniroma3.acmeedu.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity
public class Centro {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(nullable = false)
	private String nome;
	@Column
	private String indirizzo;
	@Column
	private String email;
	@Column
	private String telefono;
	@Column
	private int capienza_max;
	@OneToOne
	private Responsabile responsabileCentro;
	@OneToMany
	@JoinColumn(name = "centro_id")
	private List<Attivita> attivitàCentro = new ArrayList<>();
	@ManyToMany
	private List<Allievo> allieviAffiliati = new ArrayList<>();

	public List<Attivita> getAttivitàCentro() {
		return attivitàCentro;
	}

	public void setAttivitàCentro(List<Attivita> attivitàCentro) {
		this.attivitàCentro = attivitàCentro;
	}

	public Centro(String nome, String email, String telefono, int capienza_max) {
		this.nome = nome;
		this.email = email;
		this.telefono = telefono;
		this.capienza_max = capienza_max;
	}
	
	public Responsabile getResponsabileCentro() {
		return responsabileCentro;
	}

	public void setResponsabileCentro(Responsabile responsabileCentro) {
		this.responsabileCentro = responsabileCentro;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public int getCapienza_max() {
		return capienza_max;
	}

	public void setCapienza_max(int capienza_max) {
		this.capienza_max = capienza_max;
	}

	public Long getId() {
		return id;
	}
}
